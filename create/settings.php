<?php

	if(!isset($_SESSION['user_validity'])){
		
		
		require "classes/User.php";
		require "classes/Display.php";
	
		$u = new User;
		
		if ($_SESSION['user_validity'] == true){
			
			$display = new Display;
			
		}else{
			
			header ('Location: info.php');
		
		}
		
	}else{
		
		header ('Location: info.php');
		
	}


?>

<!DOCTYPE html>
<html>
    <head>
  
	<title>mCRM</title>
    <meta charset="utf-8">
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
	<link rel="shortcut icon" type="image/png" href="images/imageedit_1_6415835376.png"/>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="js/jQueryGoogle.js"></script>
	
	
	
	<!--<meta http-equiv="refresh" content="10" > -->
	<!--<script src="http://code.jquery.com/jquery-latest.js"></script>-->
	<!--<script src="js/refresh.js"></script>-->
	
	</script>
    </head>
	<body>
	    
    	<div id="header">
		
        	<div class="wrapper">
        	    
        		<div id="logo">
        			<img src="images/logo.png" alt="logo">
        		</div> <!-- end #logo-->
        
        		<div id="nav">
        			<ul>
					
						<?php
					
							$display->display_navigation_bar();
					
						?>
						
        			</ul>
        		</div> <!-- end #nav-->
        		
        	</div> <!-- end #header .wrapper-->
        	
        </div> <!-- end #header -->
		
		<div class="wrapper wrapper-main">
		
			<div id="main">
			
				<?php
				
					$display->display_upload_equip_form();
					
					
				?>	
				<div class ="e_table">
				
					<?php
					
						$display->display_upload_equip_table();
					
					?>	
				
				</div>
		
			</div> <!-- end #main -->
			
		</div> <!-- end #main .wrapper-->
		
		
		<div id="footer">
		
        	<div class="wrapper">
			
			
			</div> <!-- end #footer .wrapper-->
        	
        </div> <!-- end #footer -->

	
	</body>
</html>