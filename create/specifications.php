<?php

	if(!isset($_SESSION['user_validity'])){
		
		
		require "classes/User.php";
		require "classes/Display.php";
	
		$u = new User;
		
		if ($_SESSION['user_validity'] == true){
			
			$display = new Display;
			$display->choose_month_spec();
			
		}else{
			
			header ('Location: info.php');
		
		}
		
	}else{
		
		header ('Location: info.php');
		
	}


?>


<!DOCTYPE html>
<html>
    <head>
  
	<title>mCRM</title>
    <meta charset="utf-8">

    <link rel="shortcut icon" type="image/png" href="images/imageedit_1_6415835376.png"/>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="js/jQueryGoogle.js"></script>
	<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>-->
	
	
	<!--<meta http-equiv="refresh" content="10" > -->
	<!--<script src="http://code.jquery.com/jquery-latest.js"></script>-->
	<!--<script src="js/refresh.js"></script>-->

	
    </head>
	<body>
	    
    	<div id="header">
		
        	<div class="wrapper">
        	    
        		<div id="logo">
        			<img src="images/logo.png" alt="logo">
        		</div> <!-- end #logo-->
        
        		<div id="nav">
        			<ul>
					
						<?php
					
							$display->display_navigation_bar();
					
						?>
						
        			</ul>
        		</div> <!-- end #nav-->
        		
        	</div> <!-- end #header .wrapper-->
        	
        </div> <!-- end #header -->
		
		<div class="wrapper wrapper-main">
		
			<div id="main">
			
				
				
				<div class="button">
					
					<?php
					
						$display->display_choose_button_spec();
						
					?>
					
				</div><!-- end #button -->
				
				<div class="table-title">
					
					<?php
					
						$display->display_table_sumary_title();
					
					?>
					
				</div><!-- end #table-title -->
				
				
				
				
				<input id="inputfilter" type="text"></input>
				
				<div id="table">
				
					
					
					<?php
					
						
						$display->display_specifications()
					
					
					?>
					
				</div><!-- end #table -->

				<div class="button">
					
					<?php
					
						
						$display->display_export_button_spec();
						
					?>
					
				</div><!-- end #button -->
				
				
				
		
			</div> <!-- end #main -->
			
		</div> <!-- end #main .wrapper-->
		
		
		<div id="footer">
		
        	<div class="wrapper">
			
			
			</div> <!-- end #footer .wrapper-->
        	
        </div> <!-- end #footer -->
		
	<script type="text/javascript">
	$(document).ready(function(){
		$("#inputfilter").keyup(function(){
			filter = new RegExp($(this).val(),'i');
			$("#table tbody tr").filter(function(){
				$(this).each(function(){
					found = false;
					$(this).children().each(function(){
						content = $(this).html();
						if(content.match(filter))
						{
							found = true
						}
					});
					if(!found)
					{
						$(this).hide();
					}
					else
					{
						$(this).show();
					}
				});
			});
		});
	});
	</script>
	</body>
</html>