<?php

class Display{
	
	public $table_title;
	public $show_month;
	
	
	
	
	function display_navigation_bar(){
		
		$sid = $_SESSION['sid'];
		$user = $_SESSION['user'];
		$date = date('m_y');
		
		
		echo '
		
			<ul>
				<li><a href="settings.php?sid='.$sid.'&user='.$user.'">PODEŠAVANJA</a></li>
				<li><a href="specifications.php?sid='.$sid.'&month='.$date.'&user='.$user.'">SPECIFIKACIJE</a></li>
				<li><a href="sumup.php?sid='.$sid.'&month='.$date.'&user='.$user.'">KLIJENTI</a></li>
				<li><a href="index.php?sid='.$sid.'&user='.$user.'">OBJEKTI</a></li>
				
			</ul>

		
		';

		
	}
	
	function display_table(){
		
		
		require "classes/Processing.php";
		$processing = new Processing;
		
		
		echo '	
		
			<div class="tableFixHead ">
				<table class="mainTable">
					<thead>
						
							<tr>
								<th>Redni br</th>
								<th>Klijent</th>
								<th>Registracija</th> 
								<th>Marka i model vozila</th>
								<th>GPS uredjaj</th>
								<th>Dodatna oprema</th>
								<th>Roaming</th>
								<th>Datum ugradnje</th>
								<th>Akitvan/neaktivan</th>
							</tr>
						
						
					</thead>
					' ;
				
		
		
		
		$count = 1;
		foreach($processing->new_arr as $new){
			
			
			
					echo '<tbody>
						<tr>
							<td>'.$count++.'</td>
							<td>'.$new['customer'].'</td>
							<td>'.$new['registration'].'</td>
							<td>'.$new['brand_model'].'</td>
							<td>'.$new['gps_device'].'</td>
							<td>'.$new['equip'].'</td>
							<td>'.$new['roaming'].'</td>
							<td>'.$new['inst_date'].'</td>
							<td>'.$new['active'].'</td>
						</tr>
		
					</tbody>';
					
		}
			
		
		echo '		<tfoot>
						<!--<tr>
							<td colspan="8">
								<div class="links"><a href=#></a> <a class="active" href="">1</a> <a href="">2</a> <a href="">3</a> <a href="">4</a> <a href="#">&raquo;</a>
								</div>
							</td>
						</tr>-->
					</tfoot>
					
				</table>
				
				</div>
				' ;
		
		
		
	}
	
	function display_import_button(){
		
		$sid = $_GET['sid'];
		$user = $_SESSION['user'];
		
		
		echo 	'	
					<form action="processing.php?sid='.$sid.'&user='.$user.'" method="post" enctype="multipart/form-data">
					
						<input type="submit" value="Kreiraj licence" name="insert_licences" class="button1"></input>

					</form>
		
				';
	}
	
	function display_export_button_sumup(){
		
		$sid = $_GET['sid'];
		$month = '&month='.$_GET['month'];
		$res = $sid.$month;
		$user = $_GET['user'];
		
		echo 	'	
					<form action="processing.php?sid='.$res.'&user='.$user.'" method="post" enctype="multipart/form-data">
					
						<input type="submit" value="Exportuj u excel" name="excel_export_sumup" class="button1"></input>
						
					</form>
		
				';
		
	}
	
	
	
	function display_export_button_non_valid_units(){
		
		$sid = $_GET['sid'];
		
		
		echo 	'	
					<form action="processing.php?sid='.$sid.'" method="post" enctype="multipart/form-data">
					
						<input type="submit" value="Exportuj nevalidna vozila" name="excel_export_non_valid_units" class="button1"></input>
						
					</form>
		
				';
		
	}
	
	function display_export_button_main(){
		
		$sid = $_GET['sid'];
		$user = $_GET['user'];
		
		echo 	'	
					<form action="processing.php?sid='.$sid.'&user='.$user.'" method="post" enctype="multipart/form-data">
					
						<input type="submit" value="Exportuj listu u excel" name="excel_export_main" class="button1"></input>
						<input type="submit" value="Kreiraj specifikacije" name="excel_export_main_by_client" class="button1"></input>
						
					</form>
		
				';
		
	}
	
	
	
	
	
	function display_choose_button(){
		
		$sid = $_GET['sid'];
		$month = $_GET['month'];
		$user = $_GET['user'];
		
		$list_num = substr($month, 0, 2);
		
		$months = array(
			'1' => 'Januar',
			'2' => 'Februar',
			'3' => 'Mart',
			'4' => 'April',
			'5' => 'Maj',
			'6' => 'Jun',
			'7' => 'Jul',
			'8' => 'Avgust',
			'9' => 'Septembar',
			'10' => 'Oktobar',
			'11' => 'Novembar',
			'12' => 'Decembar'
		);
		
		
		foreach($months as $k => $v){

			if($k == $list_num){
				
				$new_show_month = $v;
				
			}
			
		}
		

		
		
		echo 	'	
					<form action="sumup.php?sid='.$sid.'&user='.$user.'" method="post" enctype="multipart/form-data">
					
						
						<select name="month">
							<option value="'.$list_num.'">'.$new_show_month.'</option>
							<option value="1">Januar</option>
							<option value="2">Februar</option>
							<option value="3">Mart</option>
							<option value="4">April</option>
							<option value="5">Maj</option>
							<option value="6">Jun</option>
							<option value="7">Jul</option>
							<option value="8">Avgust</option>
							<option value="9">Septembar</option>
							<option value="10">Oktobar</option>
							<option value="11">Novembar</option>
							<option value="12">Decembar</option>
							
						</select>
						
						<select name="year">
							
							<option value="18">2018</option>
							<option value="19">2019</option>
						</select>
						<input type="submit" value="odaberi mesec" name="choose_month" class="button1"></input>
					</form>
		
				';
		
		
	}
	
	function display_choose_button_spec(){
		
		$sid = $_GET['sid'];
		$month = $_GET['month'];
		$user = $_GET['user'];
		
		$list_num = substr($month, 0, 2);
		
		$months = array(
			'1' => 'Januar',
			'2' => 'Februar',
			'3' => 'Mart',
			'4' => 'April',
			'5' => 'Maj',
			'6' => 'Jun',
			'7' => 'Jul',
			'8' => 'Avgust',
			'9' => 'Septembar',
			'10' => 'Oktobar',
			'11' => 'Novembar',
			'12' => 'Decembar'
		);
		
		
		foreach($months as $k => $v){

			if($k == $list_num){
				
				$new_show_month = $v;
				
			}
			
		}
		
		
		
		echo 	'	
					<form action="specifications.php?sid='.$sid.'&user='.$user.'" method="post" enctype="multipart/form-data">
					
						
						<select name="month">
							<option value="'.$list_num.'">'.$new_show_month.'</option>
							<option value="1">Januar</option>
							<option value="2">Februar</option>
							<option value="3">Mart</option>
							<option value="4">April</option>
							<option value="5">Maj</option>
							<option value="6">Jun</option>
							<option value="7">Jul</option>
							<option value="8">Avgust</option>
							<option value="9">Septembar</option>
							<option value="10">Oktobar</option>
							<option value="11">Novembar</option>
							<option value="12">Decembar</option>
						</select>
						<select name="year">
							
							<option value="18">2018</option>
							<option value="19">2019</option>
						</select>
						<input type="submit" value="odaberi mesec" name="choose_month" class="button1"></input>
					</form>
		
				';
		
		
	}
	
	
	function display_table_sumary_title(){
		
		$month = $_GET['month'];
		$months = array(
			'1' => 'JANUAR',
			'2' => 'FEBRUAR',
			'3' => 'MART',
			'4' => 'APRIL',
			'5' => 'MAJ',
			'6' => 'JUN',
			'7' => 'JUL',
			'8' => 'AVGUST',
			'9' => 'SEPTEMBAR',
			'10' => 'OKTOBAR',
			'11' => 'NOVEMBAR',
			'12' => 'DECEMBAR'
		);
		
		
		foreach($months as $k => $v){

			if($k == $month){
				
				$this->show_month = $v;
				
			}
			
		}
		
		
		echo '
		
			<h3>'.$this->show_month.'</h3>
			
		';
		
	}
	
	
	
	function display_table_sumary(){
		
		require "config.php";
		$month = $_GET['month'];
		$user = $_GET['user'];
		
		//$date = $month.'_'.date('y');
		
		$check_database = $conn->query("SHOW TABLES LIKE '".$month."_licence'")->fetchAll();
		
		
		if(!empty($check_database)){
		
			// Connect to mySQL
			require 'configMySQLi.php';
			
			// Check does table equipment_list have content
			$equipment_list_check = mysqli_query($con,"SELECT name FROM equipment_list ORDER BY id ASC");
		
			
			
			
			if(mysqli_num_rows($equipment_list_check) != 0){
		
		
				echo '	
						
						<div class="tableFixHead ">
						<table id="myInput" class="mainTable">
							<thead>
								
									<tr>
										<th>	Redni br	</th>
										<th>	Klijent	</th>
										<th>	Broj licenci	</th>
										<th>	Broj roaming-a	</th>';
										
									
									while ($line = mysqli_fetch_assoc($equipment_list_check)) {
										
										
										foreach ($line as $cell) {
											
											echo "<th>" . htmlspecialchars($cell) . "</th>";
											
										}
										
										
										foreach ($line as $cell1) {
											
										
											$equip_list[] = strtolower(preg_replace('/\s+/', '_', str_replace('-', '_', str_replace('/', '_', str_replace('.', '_', str_replace('+', '_plus', str_replace('/', '_', htmlspecialchars($cell1) )))))));
											
										}
										
										
									}
									
									
									foreach($equip_list as $new_e_l){
						
										$insert_columns[] = $new_e_l;
										
									}
									
									$insert_columns = implode(",",$insert_columns);
									
									
									
									'</tr>
		
							</thead>
							' ;
						
				mysqli_set_charset($con,'utf8');
				
				$data = mysqli_query($con,"SELECT customer,num_of_licences,roaming,$insert_columns
				FROM `".$month."_licence` LEFT JOIN `".$month."_licence_equip` ON customera = customer ORDER BY customer ASC");
				
				
				$count = 1;
				while($row = mysqli_fetch_assoc($data)){
					
							
					
							echo '<tbody>
								<tr>
									<td>'.$count++.'</td>
									<td>'.$row['customer'].'</td>
									<td>'.$row['num_of_licences'].'</td>
									<td>'.$row['roaming'].'</td>';
									
									foreach($equip_list as $new_equip_list){
										
										echo '<td>'.$row[''.$new_equip_list.''].'</td>';
										
									}
									
									
							echo	'</tbody>';
				
				}
					
				
				echo '		<tfoot>
								<!--<tr>
									<td colspan="8">
										<div class="links"><a href=#></a> <a class="active" href="">1</a> <a href="">2</a> <a href="">3</a> <a href="">4</a> <a href="#">&raquo;</a>
										</div>
									</td>
								</tr>-->
							</tfoot>
							
						</table>
						
						</div>
						' ;
				
				$_SESSION['database_validity'] = true;
				
			}else{
				
				
				$sid = $_SESSION['sid'];
				
				ob_start();
				header("Refresh: 0.1;index.php?sid=$sid&user=$user");
				echo "<script type='text/javascript'>alert('Fajl sa dodatnom opremom nije uploadovan')</script>";
				ob_end_flush();
				
				
			}


			
		}else{
			
			echo "Nema podataka za odabrani mesec";
			$_SESSION['database_validity'] = false;
			
		}
		
	}
	
	function choose_month(){
		
		if(isset($_POST['choose_month'])){
			
			
			$sid = $_GET['sid'];
			$month = $_POST['month'];
			$year = $_POST['year'];
			$res = $month.'_'.$year;
			$user = $_GET['user'];

			header("Location: sumup.php?sid=$sid&month=$res&user=$user");
			
			
		}
		
		
	}
	
	function choose_month_spec(){
		
		if(isset($_POST['choose_month'])){
			
			
			$sid = $_GET['sid'];
			$month = $_POST['month'];
			$year = $_POST['year'];
			$res = $month.'_'.$year;
			$user = $_GET['user'];

			header("Location: specifications.php?sid=$sid&month=$res&user=$user");
			
			
		}

		
	}
	
	function display_export_button_spec(){
		
		$sid = $_GET['sid'];
		$month = $_GET['month'];
		$user = $_GET['user'];
		
		
		echo 	'	
					<form action="processing.php?sid='.$sid.'&month='.$month.'&user='.$user.'" method="post" enctype="multipart/form-data">
					
						<input type="submit" value="Exportuj specifikacije" name="excel_export_spec" class="button1"></input>

					</form>
		
				';
		
	}
	
	
	function display_specifications(){
		
		
		$month = $_GET['month'];
		$folder_name = 'downloads/'.$month;
		
		if (file_exists($folder_name)){
			
			$dir_content = array_slice(scandir('downloads/'.$month),2);
			
			//print_r ($dir_content);
			
			foreach ($dir_content as $d_k){
				
				
				$d_k1 = urldecode(substr($d_k, 0, -5));
				$d_k3 = urlencode($d_k);
				
				
				echo '<a href="downloads/'.$month.'/'.$d_k3.'"><strong>'.$d_k1.'</strong></a><br>';
				
			}
			
		}else{
			
			echo "Nema podataka za odabrani mesec";
			
		}
	

		
	
		
	}
	
	function display_upload_equip_form(){
		
		$sid = $_GET['sid'];
		$user = $_GET['user'];
		
		echo '<form action="processing.php?sid='.$sid.'&user='.$user.'" method="post" enctype="multipart/form-data">
			
			<p>Odaberi listu dodatne opreme</p>
			<br>
			<input type="file" value="Odadberite fajl" name="import_csv"></input>
			<br>
			<br>
			<input type="submit" value="Uploaduj listu" name="upload_equip_list" class="button1"></input>
			
		</form>';
	
		
		
		
	}
	
	
	function display_upload_equip_table(){
		
		// Connect to mySQL
		require 'configMySQLi.php';
		
		// Check does table equipment_list have content
		$equipment_list_check = mysqli_query($con,"SELECT name FROM equipment_list");
	
		if(mysqli_num_rows($equipment_list_check) != 0){
	
	
			echo '	
					
					<div class="tableFixHead ">
					<table id="myInput" class="mainTable">
						<thead>
							
								<tr>
									<th>	Redni broj	</th>
									<th>	Naziv opreme	</th>
								</tr>
	
						</thead>
						' ;
					
			mysqli_set_charset($con,'utf8');
			
			$data = mysqli_query($con,"SELECT name FROM equipment_list ORDER BY name ASC");
			
			$count = 1;
			while($row = mysqli_fetch_assoc($data)){
				
						
				
						echo '<tbody>
								<tr>
									<td>'.$count++.'</td>
									<td>'.$row['name'].'</td>
								</tr>
							</tbody>';
			
			}
				
			
			echo '		<tfoot>
							<!--<tr>
								<td colspan="8">
									<div class="links"><a href=#></a> <a class="active" href="">1</a> <a href="">2</a> <a href="">3</a> <a href="">4</a> <a href="#">&raquo;</a>
									</div>
								</td>
							</tr>-->
						</tfoot>
						
					</table>
					
					</div>
					';



		}
	}
	
	
	
}




?>