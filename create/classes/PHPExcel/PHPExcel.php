<?php

/** PHPExcel root directory */
if (!defined('PHPEXCEL_ROOT')) {
    define('PHPEXCEL_ROOT', dirname(__FILE__) . '/');
    require(PHPEXCEL_ROOT . 'PHPExcel/Autoloader.php');
}

/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt    LGPL
 * @version    ##VERSION##, ##DATE##
 */
class PHPExcel
{
    /**
     * Unique ID
     *
     * @var string
     */
    private $uniqueID;

    /**
     * Document properties
     *
     * @var PHPExcel_DocumentProperties
     */
    private $properties;

    /**
     * Document security
     *
     * @var PHPExcel_DocumentSecurity
     */
    private $security;

    /**
     * Collection of Worksheet objects
     *
     * @var PHPExcel_Worksheet[]
     */
    private $workSheetCollection = array();

    /**
     * Calculation Engine
     *
     * @var PHPExcel_Calculation
     */
    private $calculationEngine;

    /**
     * Active sheet index
     *
     * @var integer
     */
    private $activeSheetIndex = 0;

    /**
     * Named ranges
     *
     * @var PHPExcel_NamedRange[]
     */
    private $namedRanges = array();

    /**
     * CellXf supervisor
     *
     * @var PHPExcel_Style
     */
    private $cellXfSupervisor;

    /**
     * CellXf collection
     *
     * @var PHPExcel_Style[]
     */
    private $cellXfCollection = array();

    /**
     * CellStyleXf collection
     *
     * @var PHPExcel_Style[]
     */
    private $cellStyleXfCollection = array();

    /**
    * hasMacros : this workbook have macros ?
    *
    * @var bool
    */
    private $hasMacros = false;

    /**
    * macrosCode : all macros code (the vbaProject.bin file, this include form, code,  etc.), null if no macro
    *
    * @var binary
    */
    private $macrosCode;
    /**
    * macrosCertificate : if macros are signed, contains vbaProjectSignature.bin file, null if not signed
    *
    * @var binary
    */
    private $macrosCertificate;

    /**
    * ribbonXMLData : null if workbook is'nt Excel 2007 or not contain a customized UI
    *
    * @var null|string
    */
    private $ribbonXMLData;

    /**
    * ribbonBinObjects : null if workbook is'nt Excel 2007 or not contain embedded objects (picture(s)) for Ribbon Elements
    * ignored if $ribbonXMLData is null
    *
    * @var null|array
    */
    private $ribbonBinObjects;

    /**
    * The workbook has macros ?
    *
    * @return boolean true if workbook has macros, false if not
    */
    public function hasMacros()
    {
        return $this->hasMacros;
    }

    /**
    * Define if a workbook has macros
    *
    * @param boolean $hasMacros true|false
    */
    public function setHasMacros($hasMacros = false)
    {
        $this->hasMacros = (bool) $hasMacros;
    }

    /**
    * Set the macros code
    *
    * @param string $MacrosCode string|null
    */
    public function setMacrosCode($MacrosCode = null)
    {
        $this->macrosCode=$MacrosCode;
        $this->setHasMacros(!is_null($MacrosCode));
    }

    /**
    * Return the macros code
    *
    * @return string|null
    */
    public function getMacrosCode()
    {
        return $this->macrosCode;
    }

    /**
    * Set the macros certificate
    *
    * @param string|null $Certificate
    */
    public function setMacrosCertificate($Certificate = null)
    {
        $this->macrosCertificate=$Certificate;
    }

    /**
    * Is the project signed ?
    *
    * @return boolean true|false
    */
    public function hasMacrosCertificate()
    {
        return !is_null($this->macrosCertificate);
    }

    /**
    * Return the macros certificate
    *
    * @return string|null
    */
    public function getMacrosCertificate()
    {
        return $this->macrosCertificate;
    }

    /**
    * Remove all macros, certificate from spreadsheet
    *
    */
    public function discardMacros()
    {
        $this->hasMacros=false;
        $this->macrosCode=null;
        $this->macrosCertificate=null;
    }

    /**
    * set ribbon XML data
    *
    */
    public function setRibbonXMLData($Target = null, $XMLData = null)
    {
        if (!is_null($Target) && !is_null($XMLData)) {
            $this->ribbonXMLData = array('target' => $Target, 'data' => $XMLData);
        } else {
            $this->ribbonXMLData = null;
        }
    }

    /**
    * retrieve ribbon XML Data
    *
    * return string|null|array
    */
    public function getRibbonXMLData($What = 'all') //we need some constants here...
    {
        $ReturnData = null;
        $What = strtolower($What);
        switch ($What){
            case 'all':
                $ReturnData = $this->ribbonXMLData;
                break;
            case 'target':
            case 'data':
                if (is_array($this->ribbonXMLData) && array_key_exists($What, $this->ribbonXMLData)) {
                    $ReturnData = $this->ribbonXMLData[$What];
                }
                break;
        }

        return $ReturnData;
    }

    /**
    * store binaries ribbon objects (pictures)
    *
    */
    public function setRibbonBinObjects($BinObjectsNames = null, $BinObjectsData = null)
    {
        if (!is_null($BinObjectsNames) && !is_null($BinObjectsData)) {
            $this->ribbonBinObjects = array('names' => $BinObjectsNames, 'data' => $BinObjectsData);
        } else {
            $this->ribbonBinObjects = null;
        }
    }
    /**
    * return the extension of a filename. Internal use for a array_map callback (php<5.3 don't like lambda function)
    *
    */
    private function getExtensionOnly($ThePath)
    {
        return pathinfo($ThePath, PATHINFO_EXTENSION);
    }

    /**
    * retrieve Binaries Ribbon Objects
    *
    */
    public function getRibbonBinObjects($What = 'all')
    {
        $ReturnData = null;
        $What = strtolower($What);
        switch($What) {
            case 'all':
                return $this->ribbonBinObjects;
                break;
            case 'names':
            case 'data':
                if (is_array($this->ribbonBinObjects) && array_key_exists($What, $this->ribbonBinObjects)) {
                    $ReturnData=$this->ribbonBinObjects[$What];
                }
                break;
            case 'types':
                if (is_array($this->ribbonBinObjects) &&
                    array_key_exists('data', $this->ribbonBinObjects) && is_array($this->ribbonBinObjects['data'])) {
                    $tmpTypes=array_keys($this->ribbonBinObjects['data']);
                    $ReturnData = array_unique(array_map(array($this, 'getExtensionOnly'), $tmpTypes));
                } else {
                    $ReturnData=array(); // the caller want an array... not null if empty
                }
                break;
        }
        return $ReturnData;
    }

    /**
    * This workbook have a custom UI ?
    *
    * @return boolean true|false
    */
    public function hasRibbon()
    {
        return !is_null($this->ribbonXMLData);
    }

    /**
    * This workbook have additionnal object for the ribbon ?
    *
    * @return boolean true|false
    */
    public function hasRibbonBinObjects()
    {
        return !is_null($this->ribbonBinObjects);
    }

    /**
     * Check if a sheet with a specified code name already exists
     *
     * @param string $pSheetCodeName  Name of the worksheet to check
     * @return boolean
     */
    public function sheetCodeNameExists($pSheetCodeName)
    {
        return ($this->getSheetByCodeName($pSheetCodeName) !== null);
    }

    /**
     * Get sheet by code name. Warning : sheet don't have always a code name !
     *
     * @param string $pName Sheet name
     * @return PHPExcel_Worksheet
     */
    public function getSheetByCodeName($pName = '')
    {
        $worksheetCount = count($this->workSheetCollection);
        for ($i = 0; $i < $worksheetCount; ++$i) {
            if ($this->workSheetCollection[$i]->getCodeName() == $pName) {
                return $this->workSheetCollection[$i];
            }
        }

        return null;
    }

     /**
     * Create a new PHPExcel with one Worksheet
     */
    public function __construct()
    {
        $this->uniqueID = uniqid();
        $this->calculationEngine = new PHPExcel_Calculation($this);

        // Initialise worksheet collection and add one worksheet
        $this->workSheetCollection = array();
        $this->workSheetCollection[] = new PHPExcel_Worksheet($this);
        $this->activeSheetIndex = 0;

        // Create document properties
        $this->properties = new PHPExcel_DocumentProperties();

        // Create document security
        $this->security = new PHPExcel_DocumentSecurity();

        // Set named ranges
        $this->namedRanges = array();

        // Create the cellXf supervisor
        $this->cellXfSupervisor = new PHPExcel_Style(true);
        $this->cellXfSupervisor->bindParent($this);

        // Create the default style
        $this->addCellXf(new PHPExcel_Style);
        $this->addCellStyleXf(new PHPExcel_Style);
    }

    /**
     * Code to execute when this worksheet is unset()
     *
     */
    public function __destruct()
    {
        $this->calculationEngine = null;
        $this->disconnectWorksheets();
    }

    /**
     * Disconnect all worksheets from this PHPExcel workbook object,
     *    typically so that the PHPExcel object can be unset
     *
     */
    public function disconnectWorksheets()
    {
        $worksheet = null;
        foreach ($this->workSheetCollection as $k => &$worksheet) {
            $worksheet->disconnectCells();
            $this->workSheetCollection[$k] = null;
        }
        unset($worksheet);
        $this->workSheetCollection = array();
    }

    /**
     * Return the calculation engine for this worksheet
     *
     * @return PHPExcel_Calculation
     */
    public function getCalculationEngine()
    {
        return $this->calculationEngine;
    }    //    function getCellCacheController()

    /**
     * Get properties
     *
     * @return PHPExcel_DocumentProperties
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * Set properties
     *
     * @param PHPExcel_DocumentProperties    $pValue
     */
    public function setProperties(PHPExcel_DocumentProperties $pValue)
    {
        $this->properties = $pValue;
    }

    /**
     * Get security
     *
     * @return PHPExcel_DocumentSecurity
     */
    public function getSecurity()
    {
        return $this->security;
    }

    /**
     * Set security
     *
     * @param PHPExcel_DocumentSecurity    $pValue
     */
    public function setSecurity(PHPExcel_DocumentSecurity $pValue)
    {
        $this->security = $pValue;
    }

    /**
     * Get active sheet
     *
     * @return PHPExcel_Worksheet
     *
     * @throws PHPExcel_Exception
     */
    public function getActiveSheet()
    {
        return $this->getSheet($this->activeSheetIndex);
    }

    /**
     * Create sheet and add it to this workbook
     *
     * @param  int|null $iSheetIndex Index where sheet should go (0,1,..., or null for last)
     * @return PHPExcel_Worksheet
     * @throws PHPExcel_Exception
     */
    public function createSheet($iSheetIndex = null)
    {
        $newSheet = new PHPExcel_Worksheet($this);
        $this->addSheet($newSheet, $iSheetIndex);
        return $newSheet;
    }

    /**
     * Check if a sheet with a specified name already exists
     *
     * @param  string $pSheetName  Name of the worksheet to check
     * @return boolean
     */
    public function sheetNameExists($pSheetName)
    {
        return ($this->getSheetByName($pSheetName) !== null);
    }

    /**
     * Add sheet
     *
     * @param  PHPExcel_Worksheet $pSheet
     * @param  int|null $iSheetIndex Index where sheet should go (0,1,..., or null for last)
     * @return PHPExcel_Worksheet
     * @throws PHPExcel_Exception
     */
    public function addSheet(PHPExcel_Worksheet $pSheet, $iSheetIndex = null)
    {
        if ($this->sheetNameExists($pSheet->getTitle())) {
            throw new PHPExcel_Exception(
                "Workbook already contains a worksheet named '{$pSheet->getTitle()}'. Rename this worksheet first."
            );
        }

        if ($iSheetIndex === null) {
            if ($this->activeSheetIndex < 0) {
                $this->activeSheetIndex = 0;
            }
            $this->workSheetCollection[] = $pSheet;
        } else {
            // Insert the sheet at the requested index
            array_splice(
                $this->workSheetCollection,
                $iSheetIndex,
                0,
                array($pSheet)
            );

            // Adjust active sheet index if necessary
            if ($this->activeSheetIndex >= $iSheetIndex) {
                ++$this->activeSheetIndex;
            }
        }

        if ($pSheet->getParent() === null) {
            $pSheet->rebindParent($this);
        }

        return $pSheet;
    }

    /**
     * Remove sheet by index
     *
     * @param  int $pIndex Active sheet index
     * @throws PHPExcel_Exception
     */
    public function removeSheetByIndex($pIndex = 0)
    {

        $numSheets = count($this->workSheetCollection);
        if ($pIndex > $numSheets - 1) {
            throw new PHPExcel_Exception(
                "You tried to remove a sheet by the out of bounds index: {$pIndex}. The actual number of sheets is {$numSheets}."
            );
        } else {
            array_splice($this->workSheetCollection, $pIndex, 1);
        }
        // Adjust active sheet index if necessary
        if (($this->activeSheetIndex >= $pIndex) &&
            ($pIndex > count($this->workSheetCollection) - 1)) {
            --$this->activeSheetIndex;
        }

    }

    /**
     * Get sheet by index
     *
     * @param  int $pIndex Sheet index
     * @return PHPExcel_Worksheet
     * @throws PHPExcel_Exception
     */
    public function getSheet($pIndex = 0)
    {
        if (!isset($this->workSheetCollection[$pIndex])) {
            $numSheets = $this->getSheetCount();
            throw new PHPExcel_Exception(
                "Your requested sheet index: {$pIndex} is out of bounds. The actual number of sheets is {$numSheets}."
            );
        }

        return $this->workSheetCollection[$pIndex];
    }

    /**
     * Get all sheets
     *
     * @return PHPExcel_Worksheet[]
     */
    public function getAllSheets()
    {
        return $this->workSheetCollection;
    }

    /**
     * Get sheet by name
     *
     * @param  string $pName Sheet name
     * @return PHPExcel_Worksheet
     */
    public function getSheetByName($pName = '')
    {
        $worksheetCount = count($this->workSheetCollection);
        for ($i = 0; $i < $worksheetCount; ++$i) {
            if ($this->workSheetCollection[$i]->getTitle() === $pName) {
                return $this->workSheetCollection[$i];
            }
        }

        return null;
    }

    /**
     * Get index for sheet
     *
     * @param  PHPExcel_Worksheet $pSheet
     * @return int Sheet index
     * @throws PHPExcel_Exception
     */
    public function getIndex(PHPExcel_Worksheet $pSheet)
    {
        foreach ($this->workSheetCollection as $key => $value) {
            if ($value->getHashCode() == $pSheet->getHashCode()) {
                return $key;
            }
        }

        throw new PHPExcel_Exception("Sheet does not exist.");
    }

    /**
     * Set index for sheet by sheet name.
     *
     * @param  string $sheetName Sheet name to modify index for
     * @param  int $newIndex New index for the sheet
     * @return int New sheet index
     * @throws PHPExcel_Exception
     */
    public function setIndexByName($sheetName, $newIndex)
    {
        $oldIndex = $this->getIndex($this->getSheetByName($sheetName));
        $pSheet = array_splice(
            $this->workSheetCollection,
            $oldIndex,
            1
        );
        array_splice(
            $this->workSheetCollection,
            $newIndex,
            0,
            $pSheet
        );
        return $newIndex;
    }

    /**
     * Get sheet count
     *
     * @return int
     */
    public function getSheetCount()
    {
        return count($this->workSheetCollection);
    }

    /**
     * Get active sheet index
     *
     * @return int Active sheet index
     */
    public function getActiveSheetIndex()
    {
        return $this->activeSheetIndex;
    }

    /**
     * Set active sheet index
     *
     * @param  int $pIndex Active sheet index
     * @throws PHPExcel_Exception
     * @return PHPExcel_Worksheet
     */
    public function setActiveSheetIndex($pIndex = 0)
    {
        $numSheets = count($this->workSheetCollection);

        if ($pIndex > $numSheets - 1) {
            throw new PHPExcel_Exception(
                "You tried to set a sheet active by the out of bounds index: {$pIndex}. The actual number of sheets is {$numSheets}."
            );
        } else {
            $this->activeSheetIndex = $pIndex;
        }
        return $this->getActiveSheet();
    }

    /**
     * Set active sheet index by name
     *
     * @param  string $pValue Sheet title
     * @return PHPExcel_Worksheet
     * @throws PHPExcel_Exception
     */
    public function setActiveSheetIndexByName($pValue = '')
    {
        if (($worksheet = $this->getSheetByName($pValue)) instanceof PHPExcel_Worksheet) {
            $this->setActiveSheetIndex($this->getIndex($worksheet));
            return $worksheet;
        }

        throw new PHPExcel_Exception('Workbook does not contain sheet:' . $pValue);
    }

    /**
     * Get sheet names
     *
     * @return string[]
     */
    public function getSheetNames()
    {
        $returnValue = array();
        $worksheetCount = $this->getSheetCount();
        for ($i = 0; $i < $worksheetCount; ++$i) {
            $returnValue[] = $this->getSheet($i)->getTitle();
        }

        return $returnValue;
    }

    /**
     * Add external sheet
     *
     * @param  PHPExcel_Worksheet $pSheet External sheet to add
     * @param  int|null $iSheetIndex Index where sheet should go (0,1,..., or null for last)
     * @throws PHPExcel_Exception
     * @return PHPExcel_Worksheet
     */
    public function addExternalSheet(PHPExcel_Worksheet $pSheet, $iSheetIndex = null)
    {
        if ($this->sheetNameExists($pSheet->getTitle())) {
            throw new PHPExcel_Exception("Workbook already contains a worksheet named '{$pSheet->getTitle()}'. Rename the external sheet first.");
        }

        // count how many cellXfs there are in this workbook currently, we will need this below
        $countCellXfs = count($this->cellXfCollection);

        // copy all the shared cellXfs from the external workbook and append them to the current
        foreach ($pSheet->getParent()->getCellXfCollection() as $cellXf) {
            $this->addCellXf(clone $cellXf);
        }

        // move sheet to this workbook
        $pSheet->rebindParent($this);

        // update the cellXfs
        foreach ($pSheet->getCellCollection(false) as $cellID) {
            $cell = $pSheet->getCell($cellID);
            $cell->setXfIndex($cell->getXfIndex() + $countCellXfs);
        }

        return $this->addSheet($pSheet, $iSheetIndex);
    }

    /**
     * Get named ranges
     *
     * @return PHPExcel_NamedRange[]
     */
    public function getNamedRanges()
    {
        return $this->namedRanges;
    }

    /**
     * Add named range
     *
     * @param  PHPExcel_NamedRange $namedRange
     * @return boolean
     */
    public function addNamedRange(PHPExcel_NamedRange $namedRange)
    {
        if ($namedRange->getScope() == null) {
            // global scope
            $this->namedRanges[$namedRange->getName()] = $namedRange;
        } else {
            // local scope
            $this->namedRanges[$namedRange->getScope()->getTitle().'!'.$namedRange->getName()] = $namedRange;
        }
        return true;
    }

    /**
     * Get named range
     *
     * @param  string $namedRange
     * @param  PHPExcel_Worksheet|null $pSheet Scope. Use null for global scope
     * @return PHPExcel_NamedRange|null
     */
    public function getNamedRange($namedRange, PHPExcel_Worksheet $pSheet = null)
    {
        $returnValue = null;

        if ($namedRange != '' && ($namedRange !== null)) {
            // first look for global defined name
            if (isset($this->namedRanges[$namedRange])) {
                $returnValue = $this->namedRanges[$namedRange];
            }

            // then look for local defined name (has priority over global defined name if both names exist)
            if (($pSheet !== null) && isset($this->namedRanges[$pSheet->getTitle() . '!' . $namedRange])) {
                $returnValue = $this->namedRanges[$pSheet->getTitle() . '!' . $namedRange];
            }
        }

        return $returnValue;
    }

    /**
     * Remove named range
     *
     * @param  string  $namedRange
     * @param  PHPExcel_Worksheet|null  $pSheet  Scope: use null for global scope.
     * @return PHPExcel
     */
    public function removeNamedRange($namedRange, PHPExcel_Worksheet $pSheet = null)
    {
        if ($pSheet === null) {
            if (isset($this->namedRanges[$namedRange])) {
                unset($this->namedRanges[$namedRange]);
            }
        } else {
            if (isset($this->namedRanges[$pSheet->getTitle() . '!' . $namedRange])) {
                unset($this->namedRanges[$pSheet->getTitle() . '!' . $namedRange]);
            }
        }
        return $this;
    }

    /**
     * Get worksheet iterator
     *
     * @return PHPExcel_WorksheetIterator
     */
    public function getWorksheetIterator()
    {
        return new PHPExcel_WorksheetIterator($this);
    }

    /**
     * Copy workbook (!= clone!)
     *
     * @return PHPExcel
     */
    public function copy()
    {
        $copied = clone $this;

        $worksheetCount = count($this->workSheetCollection);
        for ($i = 0; $i < $worksheetCount; ++$i) {
            $this->workSheetCollection[$i] = $this->workSheetCollection[$i]->copy();
            $this->workSheetCollection[$i]->rebindParent($this);
        }

        return $copied;
    }

    /**
     * Implement PHP __clone to create a deep clone, not just a shallow copy.
     */
    public function __clone()
    {
        foreach ($this as $key => $val) {
            if (is_object($val) || (is_array($val))) {
                $this->{$key} = unserialize(serialize($val));
            }
        }
    }

    /**
     * Get the workbook collection of cellXfs
     *
     * @return PHPExcel_Style[]
     */
    public function getCellXfCollection()
    {
        return $this->cellXfCollection;
    }

    /**
     * Get cellXf by index
     *
     * @param  int $pIndex
     * @return PHPExcel_Style
     */
    public function getCellXfByIndex($pIndex = 0)
    {
        return $this->cellXfCollection[$pIndex];
    }

    /**
     * Get cellXf by hash code
     *
     * @param  string $pValue
     * @return PHPExcel_Style|boolean False if no match found
     */
    public function getCellXfByHashCode($pValue = '')
    {
        foreach ($this->cellXfCollection as $cellXf) {
            if ($cellXf->getHashCode() == $pValue) {
                return $cellXf;
            }
        }
        return false;
    }

    /**
     * Check if style exists in style collection
     *
     * @param  PHPExcel_Style $pCellStyle
     * @return boolean
     */
    public function cellXfExists($pCellStyle = null)
    {
        return in_array($pCellStyle, $this->cellXfCollection, true);
    }

    /**
     * Get default style
     *
     * @return PHPExcel_Style
     * @throws PHPExcel_Exception
     */
    public function getDefaultStyle()
    {
        if (isset($this->cellXfCollection[0])) {
            return $this->cellXfCollection[0];
        }
        throw new PHPExcel_Exception('No default style found for this workbook');
    }

    /**
     * Add a cellXf to the workbook
     *
     * @param PHPExcel_Style $style
     */
    public function addCellXf(PHPExcel_Style $style)
    {
        $this->cellXfCollection[] = $style;
        $style->setIndex(count($this->cellXfCollection) - 1);
    }

    /**
     * Remove cellXf by index. It is ensured that all cells get their xf index updated.
     *
     * @param integer $pIndex Index to cellXf
     * @throws PHPExcel_Exception
     */
    public function removeCellXfByIndex($pIndex = 0)
    {
        if ($pIndex > count($this->cellXfCollection) - 1) {
            throw new PHPExcel_Exception("CellXf index is out of bounds.");
        } else {
            // first remove the cellXf
            array_splice($this->cellXfCollection, $pIndex, 1);

            // then update cellXf indexes for cells
            foreach ($this->workSheetCollection as $worksheet) {
                foreach ($worksheet->getCellCollection(false) as $cellID) {
                    $cell = $worksheet->getCell($cellID);
                    $xfIndex = $cell->getXfIndex();
                    if ($xfIndex > $pIndex) {
                        // decrease xf index by 1
                        $cell->setXfIndex($xfIndex - 1);
                    } elseif ($xfIndex == $pIndex) {
                        // set to default xf index 0
                        $cell->setXfIndex(0);
                    }
                }
            }
        }
    }

    /**
     * Get the cellXf supervisor
     *
     * @return PHPExcel_Style
     */
    public function getCellXfSupervisor()
    {
        return $this->cellXfSupervisor;
    }

    /**
     * Get the workbook collection of cellStyleXfs
     *
     * @return PHPExcel_Style[]
     */
    public function getCellStyleXfCollection()
    {
        return $this->cellStyleXfCollection;
    }

    /**
     * Get cellStyleXf by index
     *
     * @param integer $pIndex Index to cellXf
     * @return PHPExcel_Style
     */
    public function getCellStyleXfByIndex($pIndex = 0)
    {
        return $this->cellStyleXfCollection[$pIndex];
    }

    /**
     * Get cellStyleXf by hash code
     *
     * @param  string $pValue
     * @return PHPExcel_Style|boolean False if no match found
     */
    public function getCellStyleXfByHashCode($pValue = '')
    {
        foreach ($this->cellStyleXfCollection as $cellStyleXf) {
            if ($cellStyleXf->getHashCode() == $pValue) {
                return $cellStyleXf;
            }
        }
        return false;
    }

    /**
     * Add a cellStyleXf to the workbook
     *
     * @param PHPExcel_Style $pStyle
     */
    public function addCellStyleXf(PHPExcel_Style $pStyle)
    {
        $this->cellStyleXfCollection[] = $pStyle;
        $pStyle->setIndex(count($this->cellStyleXfCollection) - 1);
    }

    /**
     * Remove cellStyleXf by index
     *
     * @param integer $pIndex Index to cellXf
     * @throws PHPExcel_Exception
     */
    public function removeCellStyleXfByIndex($pIndex = 0)
    {
        if ($pIndex > count($this->cellStyleXfCollection) - 1) {
            throw new PHPExcel_Exception("CellStyleXf index is out of bounds.");
        } else {
            array_splice($this->cellStyleXfCollection, $pIndex, 1);
        }
    }

    /**
     * Eliminate all unneeded cellXf and afterwards update the xfIndex for all cells
     * and columns in the workbook
     */
    public function garbageCollect()
    {
        // how many references are there to each cellXf ?
        $countReferencesCellXf = array();
        foreach ($this->cellXfCollection as $index => $cellXf) {
            $countReferencesCellXf[$index] = 0;
        }

        foreach ($this->getWorksheetIterator() as $sheet) {
            // from cells
            foreach ($sheet->getCellCollection(false) as $cellID) {
                $cell = $sheet->getCell($cellID);
                ++$countReferencesCellXf[$cell->getXfIndex()];
            }

            // from row dimensions
            foreach ($sheet->getRowDimensions() as $rowDimension) {
                if ($rowDimension->getXfIndex() !== null) {
                    ++$countReferencesCellXf[$rowDimension->getXfIndex()];
                }
            }

            // from column dimensions
            foreach ($sheet->getColumnDimensions() as $columnDimension) {
                ++$countReferencesCellXf[$columnDimension->getXfIndex()];
            }
        }

        // remove cellXfs without references and create mapping so we can update xfIndex
        // for all cells and columns
        $countNeededCellXfs = 0;
        $map = array();
        foreach ($this->cellXfCollection as $index => $cellXf) {
            if ($countReferencesCellXf[$index] > 0 || $index == 0) { // we must never remove the first cellXf
                ++$countNeededCellXfs;
            } else {
                unset($this->cellXfCollection[$index]);
            }
            $map[$index] = $countNeededCellXfs - 1;
        }
        $this->cellXfCollection = array_values($this->cellXfCollection);

        // update the index for all cellXfs
        foreach ($this->cellXfCollection as $i => $cellXf) {
            $cellXf->setIndex($i);
        }

        // make sure there is always at least one cellXf (there should be)
        if (empty($this->cellXfCollection)) {
            $this->cellXfCollection[] = new PHPExcel_Style();
        }

        // update the xfIndex for all cells, row dimensions, column dimensions
        foreach ($this->getWorksheetIterator() as $sheet) {
            // for all cells
            foreach ($sheet->getCellCollection(false) as $cellID) {
                $cell = $sheet->getCell($cellID);
                $cell->setXfIndex($map[$cell->getXfIndex()]);
            }

            // for all row dimensions
            foreach ($sheet->getRowDimensions() as $rowDimension) {
                if ($rowDimension->getXfIndex() !== null) {
                    $rowDimension->setXfIndex($map[$rowDimension->getXfIndex()]);
                }
            }

            // for all column dimensions
            foreach ($sheet->getColumnDimensions() as $columnDimension) {
                $columnDimension->setXfIndex($map[$columnDimension->getXfIndex()]);
            }

            // also do garbage collection for all the sheets
            $sheet->garbageCollect();
        }
    }

    /**
     * Return the unique ID value assigned to this spreadsheet workbook
     *
     * @return string
     */
    public function getID()
    {
        return $this->uniqueID;
    }
}

class ExcelExport{	

	
	function export_main (){
		
		if(isset($_POST['excel_export_main'])){
			
			
			// Connect to mySQL
			require './configMySQLi.php';
			
			
			// Create object
			$excel = new PHPExcel();
			
			// Create sheet in workbook
			$excel->setActiveSheetIndex(0);
			
			

			// Set charset
			mysqli_set_charset($con,"utf8");
			
			// mySQL query
			$query = mysqli_query($con,"SELECT * FROM main_table");
			
			
			// Read data from mySQL and insert in to Excel
			
			$row = 2;
			$count = 1;
			
			while($data = mysqli_fetch_object($query)){
				
				
				
				
				$excel->getActiveSheet()
				
					->setCellValue('A'.$row, $count)			
					->setCellValue('B'.$row, $data->customer)
					->setCellValue('C'.$row, $data->registration)
					->setCellValue('D'.$row, $data->brand_model)
					->setCellValue('E'.$row, $data->gps_device)
					->setCellValue('F'.$row, $data->equip)
					->setCellValue('G'.$row, $data->inst_date)
					->setCellValue('H'.$row, $data->roaming)
					->setCellValue('I'.$row, $data->active);
					
				$row++;
				$count++;
				
			}
			
			// Set size of cells in Excel
			
			$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
			
			
			
			
			// Add headers in Excel
			
			$excel->getActiveSheet()
				->setCellValue('A1', 'Redni broj')
				->setCellValue('B1', 'Firma')
				->setCellValue('C1', 'Registracija')
				->setCellValue('D1', 'Marka i model')
				->setCellValue('E1', 'GPS uređaj')
				->setCellValue('F1', 'Dodatna oprema')
				->setCellValue('G1', 'Datum ugradnje')
				->setCellValue('H1', 'Roaming')
				->setCellValue('I1', 'Aktivan/neaktivan');
			
			
			// Set sheetname for Excel file
			
			
			$excel->getActiveSheet()->setTitle("Svi uniti");
			
			
			// Set name of file and Export data in Excel
			
			header('Content-Type: application/vnd-openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename="Svi uniti.xlsx"');
			header('Cache-Control: max-age=0');
			header("Pragma: no-cache");
		
			$file = PHPExcel_IOFactory::createWriter($excel,'Excel2007');
			$file->save('php://output');
			exit();
			
	
		}
	
	}
	
	
	function export_non_valid_units(){
		
		if(isset($_POST['excel_export_non_valid_units'])){
			
			
			// Get array of units with non-valid equip field
			session_start();
			$non_valid_units = $_SESSION['non_valid_units'];
			
			// Create object
			$excel = new PHPExcel();
			
			// Create sheet in workbook
			$excel->setActiveSheetIndex(0);
			
			// Read data from array and insert in to Excel
			$row = 2;
			$count = 1;
			
		
			foreach($non_valid_units as $non_units){
				
			
				$excel->getActiveSheet()
				
					->setCellValue('A'.$row, $count)			
					->setCellValue('B'.$row, $non_units['Registracija'])
					->setCellValue('C'.$row, $non_units['Nevalidna oprema']);
					
				$row++;
				$count++;
				
				
			}
			
			// Set size of cells in Excel
			$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			
			// Add headers in Excel
			$excel->getActiveSheet()
				->setCellValue('A1', 'Redni broj')
				->setCellValue('B1', 'Registracija')
				->setCellValue('C1', 'Nevalidna oprema');
			
			
			
			// Set sheetname for Excel file
			$excel->getActiveSheet()->setTitle("Vozila sa nevalidnom opremom");
			
			
			// Unset array of non-valid units
			unset($_SESSION['non_valid_units']);
			
			// Set name of file and Export data in Excel
			header('Content-Type: application/vnd-openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename="Vozila sa nevalidnom opremom.xlsx"');
			header('Cache-Control: max-age=0');
			header("Pragma: no-cache");
		
			$file = PHPExcel_IOFactory::createWriter($excel,'Excel2007');
			$file->save('php://output');
			exit();
			
			
		}
	}
	
	function export_to_excel_sumup(){
		
		if(isset($_POST['excel_export_sumup'])){
			
			$user = $_GET['user'];
			
			// Get month to export to Excel
			session_start();
			$ext_filename = $_GET['month'];
			
			
			// Connect to mySQL
			require './configMySQLi.php';
			
			//Check does database exists
			$database_validity = $_SESSION['database_validity'];
			
			
			
			if($database_validity == true){
				
				// Check does table equipment_list have content
				$equipment_list_check = mysqli_query($con,"SELECT name FROM equipment_list");
		
				if(mysqli_num_rows($equipment_list_check) != 0){
				
					
					while ($line = mysqli_fetch_assoc($equipment_list_check)) {
						
						foreach ($line as $cell) {
							
							
							$equip_list[] = $cell;
							$equip_list1[] = strtolower(preg_replace('/\s+/', '_', str_replace('-', '_', str_replace('/', '_', str_replace('.', '_', str_replace('+', '_plus', str_replace('/', '_', htmlspecialchars($cell) )))))));
							
						}
						
					}
				
					
					foreach($equip_list1 as $new_e_l1){
						
						$insert_columns1[] = $new_e_l1;
						
					}
					
					$insert_columns1 = implode(",",$insert_columns1);
					
					// Create object
					$excel = new PHPExcel();
					
					// Create sheet in workbook
					$excel->setActiveSheetIndex(0);
					
					// Set charset
					mysqli_set_charset($con,"utf8");
					
					// mySQL query
					$query = mysqli_query($con,"SELECT customer,num_of_licences,roaming,$insert_columns1
					FROM `".$ext_filename."_licence` LEFT JOIN `".$ext_filename."_licence_equip` ON customera = customer ORDER BY customer ASC"); 
					
					
					
					
					
					// Read data from mySQL and insert in to Excel
					$row = 2;
					$count = 1;
					$broj_kl = $query->num_rows;
					
					
					while($data = mysqli_fetch_object($query)){
						
						
						
						$excel->getActiveSheet()->setCellValue('A'.$row, $count)	;
						$excel->getActiveSheet()->setCellValue('A1', 'Redni broj')	;
						
						$obj_elements = get_object_vars($data);
						
						$char = 'B';
						$validation = true;
						
						foreach ($obj_elements as $elem_k => $elem_v){
							
							
							for($i=0 ; $i<$broj_kl;$i++){
								
								
								if ($validation == true){ 
								
									$excel->getActiveSheet()->setCellValue($char.'1', $elem_k)	;
									$excel->getActiveSheet()->setCellValue($char.$row, $elem_v)	;	

								}
								
							}
							
							
							$char++;
						
						}
						
						$validation = false;
						$row++;
						$count++;
						
						
					}
					
				
					// Set size of cells in Excel
					for ($char_s = 'A'; $char_s <= 'Z'; $char_s++) {
						
						
						$excel->getActiveSheet()->getColumnDimension($char_s)->setAutoSize(true);
					
					}
					
					$style = array(
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							)
						);

						$excel->getDefaultStyle()->applyFromArray($style);
					

					// Set sheetname for Excel file
					$excel->getActiveSheet()->setTitle("Licence za $ext_filename");
					
					// Set name of file and Export data in Excel
					header('Content-Type: application/vnd-openxmlformats-officedocument.spreadsheetml.sheet');
					header('Content-Disposition: attachment; filename="Licence za"'.$ext_filename.'".xlsx"');
					header('Cache-Control: max-age=0');
					header("Pragma: no-cache");
				
					$file = PHPExcel_IOFactory::createWriter($excel,'Excel2007');
					$file->save('php://output');
					exit();
					
				
				}else{
					
					$sid = $_SESSION['sid'];
					
					
					ob_start();
					header("Refresh: 0.1;sumup.php?sid=$sid&user=$user");
					echo "<script type='text/javascript'>alert('Fajl sa dodatnom opremom nije uploadovan')</script>";
					ob_end_flush();
					
					
				}
				
			}else{
				
				$date = $_GET['month'];
				$sid = $_SESSION['sid'];
			
				ob_start();
				header("Refresh: 0.1;sumup.php?sid=$sid&month=$date&user=$user");
				echo "<script type='text/javascript'>alert('Licence za odabrani mesec nisu kreirane')</script>";
				ob_end_flush();
				
				
			}
		}
	}
	
	
	function export_to_excel_spec(){
		
		if(isset($_POST['excel_export_main_by_client'])){
			
			$sid = $_GET['sid'];
			$user = $_GET['user'];
			
			// Variable for file name
			$date = date('m_y');
			
			//Check does folder exists
			$folder_path = "./downloads/".date('m_y');
			
			if(!is_dir($folder_path)){
				
				//Create folder
				mkdir($folder_path);
				
				
				// Connect to mySQL
				require './configMySQLi.php';
				
				
				// Set charset
				mysqli_set_charset($con,"utf8");
				
				// mySQL query
				$query = mysqli_query($con,"SELECT * FROM main_table");
				
				//Convert object into array for creating separate Excel specifications
				while($data = mysqli_fetch_object($query)){
					
					$customer = $data->customer;
					$new_array[$customer][] = $data;
					
				}
				
				
				// Variable for file name
				$date = date('m_y');
				
				// Validation for separate Excel specifications
				$check = false;
				
				foreach($new_array as $new_array_k => $new_array_v){
					
					
					
					
					// Variable for file name
					$file_name = urlencode($new_array_k.'_'.$date).'.xlsx';
					
					// File pathinfo
					$file_path = './downloads/'.$date.'/'.$file_name;
					
					// Create object
					$excel = new PHPExcel();
		
					// Create sheet in workbook
					$excel->setActiveSheetIndex(0);
					
					// Add data to Excel file
					$row = 2;
					$count = 1;
					
					if($check == false){
						
						foreach($new_array_v as $spec_row){
							
							$excel->getActiveSheet()
							->setCellValue('A'.$row, $count)			
							->setCellValue('B'.$row, $spec_row->customer)
							->setCellValue('C'.$row, $spec_row->registration)
							->setCellValue('D'.$row, $spec_row->brand_model)
							->setCellValue('E'.$row, $spec_row->gps_device)
							->setCellValue('F'.$row, $spec_row->equip)
							->setCellValue('G'.$row, $spec_row->inst_date)
							->setCellValue('H'.$row, $spec_row->roaming)
							->setCellValue('I'.$row, $spec_row->active);
							
							
							$row++;
							$count++;
							
							
							
						}
						
						// Add headers in Excel
						$excel->getActiveSheet()
						->setCellValue('A1', 'Redni broj')
						->setCellValue('B1', 'Klijent')
						->setCellValue('C1', 'Registracija')
						->setCellValue('D1', 'Marka i model')
						->setCellValue('E1', 'GPS uređaj')
						->setCellValue('F1', 'Dodatna oprema')
						->setCellValue('G1', 'Datum ugradnje')
						->setCellValue('H1', 'Roaming')
						->setCellValue('I1', 'Aktivan/neaktivan');
						
						// Set size of cells in Excel
						$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
						$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
						$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
						$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
						$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
						$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
						$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
						$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
						$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
						
						
						$style = array(
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							)
						);

						$excel->getDefaultStyle()->applyFromArray($style);
						
						
						
						$header_style = array(
							'font'  => array(
								'bold'  => true,
								'size'  => 12,
								'name'  => 'Calibri'
							));
						
						$excel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($header_style);
						$excel->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C0C0C0');
						
						$excel->getActiveSheet()->setTitle($new_array_k);
						
						$excel = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
						$excel->save($file_path); 
					
						
					}
					
					
				}
				
				$check = true;
				
			
				header("Location: specifications.php?sid=$sid&month=$date&user=$user");
				
			}else{
				
				$user = $_GET['user'];
				
				
				ob_start();
				header("Refresh: 0.1;specifications.php?sid=$sid&month=$date&user=$user");
				echo "<script type='text/javascript'>alert('Specifikacije za tekući mesec su već kreirane')</script>";
				ob_end_flush();
				
				
			}
		}
	}



	function export_specifications_in_zip(){
		
		if(isset($_POST['excel_export_spec'])){
			
			$month = $_GET['month'];
			
			
			$check_folder = './downloads/'.$month;
			
			//Check if folder exists
			if(is_dir($check_folder)){
			
				// Prepare variables for creating and export ZIP file
				$path = './downloads/'.$month;
				$file_name = array_slice(scandir($path),2);
				
				// Create ZIP file
				$zip_file_tmp = tempnam("/tmp", 'your_prefix');
				$zip = new ZipArchive();
				$zip->open($zip_file_tmp, ZipArchive::OVERWRITE);
				
				foreach($file_name as $new_file_name){
					
					$new_path = $path.'/'.$new_file_name;
					$zip->addFile($new_path,$new_file_name);
				}
				
				$zip->close();

				// Download ZIP-ed specifications
				$download_filename = 'Specifikacije_za_'.$month.'.zip'; 
				header("Content-Type: application/zip");
				header("Content-Length: " . filesize($zip_file_tmp));
				header("Content-Disposition: attachment; filename=\"" . $download_filename . "\"");
				readfile($zip_file_tmp);
				unlink($zip_file_tmp);
				
			}else{
				
				$date = $_GET['month'];
				$sid = $_GET['sid'];
				$user = $_GET['user'];
				
				ob_start();
				header("Refresh: 0.1;specifications.php?sid=$sid&month=$date&user=$user");
				echo "<script type='text/javascript'>alert('Specifikacije za odabrani mesec nisu kreirane')</script>";
				ob_end_flush();
				
			}
		}
	}


	function insert_equip_list_into_database(){
		
		if(isset($_POST["upload_equip_list"])){
			
			$sid = $_GET['sid'];
			$user = $_GET['user'];
			
			// Types of CSV files
			$csv_mimetypes = array(
				'text/csv',
				'application/csv',
				'text/comma-separated-values',
				'application/vnd.ms-excel',
				'application/vnd.msexcel',
			);
			
			// Check file extension
			if(in_array($_FILES["import_csv"]["type"],$csv_mimetypes)){
			
				// Check if file exists in upload form
				if($_FILES["import_csv"]["size"] > 0){
				
					// Connect to mySQL
					require 'configMySQLi.php';
					
					// Truncate old data
					$truncate_sql = "TRUNCATE TABLE `equipment_list`";
					$truncate_result = mysqli_query($con, $truncate_sql);
					
					// Set temporary name				
					$filename = $_FILES["import_csv"]["tmp_name"];		
			 
					
					// Insert CSV file into database
					$file = fopen($filename, "r");
					
					while (($getData = fgetcsv($file, 10000, ",")) !== FALSE){
					
						$sql = "INSERT INTO equipment_list (name) 
									values ('".$getData[0]."')";
							
							
						if($getData[0] != null){
				
							$result = mysqli_query($con, $sql);
						
						}
					}
					
					fclose($file);
					
					// Check does columns exists in all sum tables 
					$query_all_tables = "SHOW TABLES FROM licence WHERE Tables_in_licence LIKE '%licence_equip%'";
					$result = mysqli_query($con,$query_all_tables);
					
					$query_equp_list = "SELECT name FROM equipment_list";
					$result_equp_list = mysqli_query($con,$query_equp_list);
					
					
					while($data1 = mysqli_fetch_object($result)){
						
						
						
						while($data = mysqli_fetch_object($result_equp_list)){
						
							$edited_equip_list[] = strtolower(preg_replace('/\s+/', '_', str_replace('-', '_', str_replace('/', '_', str_replace('.', '_', str_replace('+', '_plus', str_replace('/', '_', htmlspecialchars($data->name) )))))));
							
						}
						
						
						foreach($edited_equip_list as $e_e){
							
							$table = $data1->Tables_in_licence;
							$column = $e_e;
							
							// Check does column exists
							$check_query = "SHOW COLUMNS FROM `$table` LIKE '$column'";
							$result_check_query = mysqli_query($con,$check_query);
							
							
							if($result_check_query->num_rows < 1){
								
								$column_to_insert[] = $column;
								
							}
							
						}
						
						
						if(!empty($column_to_insert)){
							
							foreach ($column_to_insert as $new_column){
								
								$insert_column_query = "ALTER TABLE `$table` ADD $new_column int(11) DEFAULT NULL";
								$execute_insert_column_query = mysqli_query($con,$insert_column_query);
							}
						}
						
					}
					
					
					ob_start();
					header("Refresh: 0.1;settings.php?sid=$sid&user=$user");
					echo "<script type='text/javascript'>alert('Fajl je uspešno unešen')</script>";
					ob_end_flush();
					
				}else{
					
					ob_start();
					header("Refresh: 0.1;settings.php?sid=$sid&user=$user");
					echo "<script type='text/javascript'>alert('Morate odabrati fajl')</script>";
					ob_end_flush();
					
				}
				
			}else{
				
				ob_start();
				header("Refresh: 0.1;settings.php?sid=$sid&user=$user");
				echo "<script type='text/javascript'>alert('Morate odabrati u CSV formatu')</script>";
				ob_end_flush();
				
			}
		}
	}






	
}



