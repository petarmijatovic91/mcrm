<?php

class Processing {
	
	public $new_arr;
	public $sid;

	
	
	function __construct(){
		
		
		require "Wialon.php";
		require "config.php";
		$wialon_api = new Wialon();
		$wialon_api->insert_sid();
		$_SESSION['sid'] = $wialon_api->sid;
		
		
		$data = $conn->query("SELECT * FROM main_table ORDER BY customer ASC")->fetchAll();
		
		
		//ARRAY ZA PRAVLJENJE KOLICINA
		
		
		
		foreach($data as $new_data){
			
			
			$new_array['customer'] = $new_data['customer'];
			$new_array['company'] = $new_data['company'];
			$new_array['registration'] = $new_data['registration'];
			$new_array['brand_model'] = $new_data['brand_model'];
			$new_array['gps_device'] = $new_data['gps_device'];
			$new_array['equip'] = $new_data['equip'];
			$new_array['inst_date'] = $new_data['inst_date'];
			$new_array['roaming'] = $new_data['roaming'];
			$new_array['active'] = $new_data['active'];
			$new_array['terminal_num'] = $new_data['terminal_num'];
			$new_array['service_num_rn'] = $new_data['service_num_rn'];
			$new_array['service'] = $new_data['service'];
			$new_array['imei'] = $new_data['imei'];
			$new_array['instalater'] = $new_data['instalater'];
			

			$this->new_arr[] = $new_array;
			unset($new_array);
			
			
			$main_customer['customer'] = $new_data['customer'];
			$main_customer['registration'] = $new_data['registration'];
			$main_customer['active'] = $new_data['active'];
			$main_customer['roaming'] = $new_data['roaming'];
			
			$equip_cistomer['customer'] = $new_data['customer'];
			$equip_cistomer['equip'] = $new_data['equip'];
			$equip_cistomer['gps_device'] = $new_data['gps_device'];
			$equip_cistomer['registration'] = $new_data['registration'];
			$equip_cistomer['active'] = $new_data['active'];
			
			$this->new_main_customer[] = $main_customer;
			$this->new_equip_cistomer[] = $equip_cistomer;
			
			unset($main_customer);
			unset($equip_cistomer);
		
		
		}

		
		
	}
	
	
	
	function insert_quantities(){
		
		if(isset($_POST['insert_licences'])){
			
			session_start();
			
			// Connect to mySQL
			require 'configMySQLi.php';
			
			// Check does table equipment_list have content
			$equipment_list_check = mysqli_query($con,"SELECT name FROM equipment_list");
		
			if(mysqli_num_rows($equipment_list_check) != 0){
			
				// Connect to mySQL
				require 'configMySQLi.php';
			
				$equip_list = array();
				array_push($equip_list,"");
				
				while ($line = mysqli_fetch_assoc($equipment_list_check)) {
					
					
					foreach ($line as $cell) {
						
					
						$equip_list[] = strtolower(preg_replace('/\s+/', '_', str_replace('-', '_', str_replace('/', '_', str_replace('.', '_', str_replace('+', '_plus', str_replace('/', '_', htmlspecialchars($cell) )))))));
						
					}
					
					
				}
				
				
				
				foreach($equip_list as $new_e_l){
					
					$insert_columns[] = "`".$new_e_l."` int(11) DEFAULT NULL,";
					
				}
				
				
				$insert_columns = array_slice($insert_columns,1);
				
				
			
				
				foreach($this->new_equip_cistomer as $i_k => $i_v){
					
					$check_e = explode(',',$i_v['equip']);
					
					$reg = $i_v['registration'];
					
					
					foreach($check_e as $new_check_e){
						
						$new_check_e_string = strtolower(preg_replace('/\s+/', '_', str_replace('-', '_', str_replace('/', '_', str_replace('.', '_', str_replace('+', '_plus', str_replace('/', '_', $new_check_e)))))));
						
						
						
						if(!in_array($new_check_e_string,$equip_list)){
							
						
							
							$non_valid_registration = $reg;
							$non_valid_item = $new_check_e_string;
							
							
							$non_valid_array['Registracija'] = $reg;
							$non_valid_array['Nevalidna oprema'] = $new_check_e;
							
							
							$non_valid_units[] = $non_valid_array;
							unset($non_valid_array);
							
							
						}
					}
				}
				
				
				if(empty($non_valid_units)){
			
					
					
					ini_set('memory_limit', -1);
					include "config.php";
					$date = date('m_y');
					
					
					$check_database = $conn->query("SHOW TABLES LIKE '".$date."_licence'")->fetchAll();
					
					if(empty($check_database)){
					
						$insert_columns = implode("",$insert_columns);
						
						
						$sql_create_equip = "CREATE TABLE IF NOT EXISTS `".$date."_licence_equip` (
											`id` int(11) NOT NULL AUTO_INCREMENT,
											`customera` varchar(255) NOT NULL,
												$insert_columns
											
											PRIMARY KEY (`id`)
											) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;";
						
						
						$sql_create_licences = "CREATE TABLE IF NOT EXISTS `".$date."_licence` (
											  `id` int(11) NOT NULL AUTO_INCREMENT,
											  `customer` varchar(255) DEFAULT NULL,
											  `num_of_licences` int(11) DEFAULT NULL,
											  `roaming` int(11) DEFAULT NULL,
											  PRIMARY KEY (`id`)
											) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;";


						$sql_create_equip_st = $conn->prepare($sql_create_equip);
						$sql_create_licences_st = $conn->prepare($sql_create_licences);
						$sql_create_equip_st->execute();
						$sql_create_licences_st->execute();
						
						
						
						
						
						
						//INSERT CUSTOMERS AND LICENCES
						foreach($this->new_main_customer as $k => $v){
							
							$customer = $v['customer'];
							$licences = $v['registration'];
							$active_licences = $v['active'];
							$roaming = $v['roaming'];
							
							if($active_licences == "da"){
								
								$import_array[$customer][] = $licences;	
								
								if($roaming == "da"){
									
									$import_array_r[$customer][] = $roaming;	
									
								}
							}
							
							
							
						}	
						
						
						if(isset($import_array_r)){
							
							foreach($import_array_r as $key_r => $value_r ){
								
								$number_of_licences_r[$key_r] = count($value_r);
							
						
							}
						}
						
						
							
							
						foreach($import_array as $key => $value ){
							
							$number_of_licences[$key] = count($value);
						
					
						}
					
						
						
					
						foreach ($number_of_licences as $num_k => $num_v){
							
							
							if(!empty($num_k)){
								
								$sql = ("INSERT INTO `".$date."_licence` (id,customer,num_of_licences)VALUES (?,?,?)");
								$stmt = $conn->prepare($sql);
								$stmt->execute([null,$num_k,$num_v]);
								
								$sql1 = ("INSERT INTO `".$date."_licence_equip` (id,customera)VALUES (?,?)");
								$stmt1 = $conn->prepare($sql1);
								$stmt1->execute([null,$num_k]);
								
								
							}

						}
						
						if(isset($number_of_licences_r)){
							foreach ($number_of_licences_r as $num_k => $num_v){
								
								
								$roam = "roaming";
								
								if(!empty($num_k)){
									
									$sql = "UPDATE `".$date."_licence` SET $roam=? WHERE customer=?";
									$conn->prepare($sql)->execute([$num_v,$num_k]);
									
								
								}

							}
						}
		
	
				
						
						//INSERT CUSTOMERS AND LICENCES
						
						
						//INSERT CUSTOMERS AND EQUIPMENT
						
						
						
						foreach($this->new_equip_cistomer as $kk => $vv){
							
							$e_customer = $vv['customer'];
							$equip = $vv['equip'];
							$gps_device = $vv['gps_device'];
							$additional_equip = explode (',',$vv['equip']);
							$active_equip = $vv['active'];
							
							
							
							array_push($additional_equip,$gps_device);
							if($active_equip == "da"){
								
								$import_array1[$e_customer][] = $additional_equip;
								
							}
							
							
							
							
						}	
						
						foreach($import_array1 as $key1 => $value1 ){
							
							
							foreach ($value1 as $v_k => $v_v){
								foreach($v_v as $v_k1 =>$v_v1){
									if(!empty($v_v1)){
										
										if($v_v1 != 'Nema podataka'){
											
											$brand_new_array[$key1][] = $v_v1;
											
										}
									}
								}
							}
						}
						
						
						
						if(isset($brand_new_array)){
							
							
							foreach($brand_new_array as $brand_new_array_k => $brand_new_array_v){
								
								$result_brand_new_array_v = array_count_values($brand_new_array_v);
								
								foreach($result_brand_new_array_v as $brand_new_array_v_k => $brand_new_array_v_v){
									
									

									
									if($brand_new_array_v_k != "/" && $brand_new_array_v_k != "Nema podataka"){
										
										$string = strtolower(preg_replace('/\s+/', '_', str_replace('-', '_', str_replace('/', '_', str_replace('.', '_', str_replace('+', '_plus', str_replace('/', '_', $brand_new_array_v_k)))))));
										
										
									
										$sql = "UPDATE `".$date."_licence_equip` SET $string=? WHERE customera=?";
										$conn->prepare($sql)->execute([$brand_new_array_v_v,$brand_new_array_k]);
										

										
									}
								}
								
							}
						}
						//INSERT CUSTOMERS AND EQUIPMENT
						
						
						$sid = $_SESSION['sid'];
						$user = $_SESSION['user'];
						
						
						header("Location: sumup.php?sid=$sid&month=$date&user=$user");
					
						
					}else{
					
						$sid = $_SESSION['sid'];
						$user = $_SESSION['user'];
						
						ob_start();
						header("Refresh: 0.1;sumup.php?sid=$sid&month=$date&user=$user");
						echo "<script type='text/javascript'>alert('Licence za tekući mesec su već unešene')</script>";
						ob_end_flush();
						
					}
					
				}else{
					
					$sid = $_SESSION['sid'];
					$user = $_SESSION['user'];
					$date = date('m_y');
					
					$_SESSION['non_valid_units'] = $non_valid_units;
					
					ob_start();
					header("Refresh: 0.1;index.php?sid=$sid&month=$date&user=$user");
					echo "<script type='text/javascript'>alert('Polje dodatne opreme za vozila nije dobro unešeno ')</script>";
					ob_end_flush();
					
					
					
				}
			
			
			}else{
				
				
				$sid = $_SESSION['sid'];
				$user = $_SESSION['user'];
				
				ob_start();
				header("Refresh: 0.1;index.php?sid=$sid&user=$user");
				echo "<script type='text/javascript'>alert('Fajl sa dodatnom opremom nije uploadovan')</script>";
				ob_end_flush();
				
				
			}
			
		}
		
		
	}
	

	
	
}




?>