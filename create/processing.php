<?php



	require "classes/Processing.php";
	require "classes/PHPExcel/PHPExcel.php";

	
	$p = new Processing;
	$e = new ExcelExport;
	
	$p->insert_quantities();
	$e->export_main();
	$e->export_non_valid_units();
	$e->export_to_excel_sumup();
	$e->export_to_excel_spec();
	$e->export_specifications_in_zip();
	$e->insert_equip_list_into_database();
	
	
	
?>
